package com.dipankar.kafka.consumer.config;

import java.util.HashMap;
import java.util.Map;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.listener.ContainerProperties;
import org.springframework.kafka.support.serializer.JsonDeserializer;


@EnableKafka
@Configuration

public class KafkaConsumerConfig {

    @Value("${kafka.server}")
    private String bootStrapServer;

    @Value("${kafka.consumer-group-id.adjudication}")
    private String consumerGroupId;

    /*
        Since we are using ObjectMapper to create the Object from JSON string value, we can have both
        ConsumerFactory<String, Object> and ConcurrentKafkaListenerContainerFactory<String, Object> as
        ConsumerFactory<String, String> and ConcurrentKafkaListenerContainerFactory<String, String>. And in that case
        VALUE_DESERIALIZER_CLASS_CONFIG value can be set as StringDeserializer.class.

        JsonSerializer allows for more straightforward handling of JSON data as the serialization and deserialization
        steps are handled automatically by Spring Kafka. However, using StringSerializer gives you more control over the
        serialization process and allows for custom serialization formats beyond JSON.
     */

    @Bean
    public ConsumerFactory<String, Object> consumerFactory()
    {
        Map<String, Object> config = new HashMap<>();
        config.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, bootStrapServer);
        config.put(ConsumerConfig.GROUP_ID_CONFIG, consumerGroupId);
        config.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
        config.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, JsonDeserializer.class);
//        config.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, false);
//        config.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
        return new DefaultKafkaConsumerFactory<>(config, new StringDeserializer(), new JsonDeserializer<>());
    }

    @Bean
    public ConcurrentKafkaListenerContainerFactory<String, Object> concurrentKafkaListenerContainerFactory()
    {
        ConcurrentKafkaListenerContainerFactory<String, Object> factory = new ConcurrentKafkaListenerContainerFactory<>();
        factory.setConsumerFactory(consumerFactory());
//        factory.getContainerProperties().setAckMode(ContainerProperties.AckMode.MANUAL_IMMEDIATE);
        return factory;
    }
}
