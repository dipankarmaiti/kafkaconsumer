package com.dipankar.kafka.consumer.service;

import com.dipankar.kafka.consumer.model.Order;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

@Component
public class ListenerService {

    private final ObjectMapper objectMapper = new ObjectMapper();

    @KafkaListener(topics = "${kafka.topic.order}", groupId = "${kafka.consumer-group-id.adjudication}")
    public void consumeMessageForAdjudication(ConsumerRecord<String, String> record ) {
        try {
            Order obj = objectMapper.readValue(record.value(), Order.class);
            System.out.println("Metadata information: Partition - " + record.partition() + " Offset - " + record.offset() + " Key - " + record.key());
            System.out.println("OrderNo: " + obj.getOrderNo() + " Adjudicated .");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /*
        Since we have configured adjudication group id in Config class, it will be used as the default group ID for all listeners
        unless overridden explicitly in the @KafkaListener annotation. So, here first listener will be used for adjudication and second
        one will be used for dispensing. This way we can still have common consumer config, but possible to create two separate listeners
        with different group id.
     */
   // @KafkaListener(topics = "${kafka.topic.order}", groupId = "${kafka.consumer-group-id.dispensing}")
    public void consumeMessageForDispensing(ConsumerRecord<String, String> record) {
        try {
            Order obj = objectMapper.readValue(record.value(), Order.class);
            System.out.println("OrderNo: " + obj.getOrderNo() + " dispensed .");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
