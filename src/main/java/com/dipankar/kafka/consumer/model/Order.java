package com.dipankar.kafka.consumer.model;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Order {
    private String orderNo;
    private String rxName;
    private String eventName;
}
